package com.squgeim.xhiftbencoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author squgeim
 */
public class BencodeList extends ArrayList<BencodeDataType> implements BencodeDataType {
    
    public void add(int x) {
        BencodeInt i = new BencodeInt(x);
        this.add(i);
    }
    
    public void add(String s) {
        BencodeString temp = new BencodeString(s);
        this.add(temp);
    }
    
    @Override
    public String getBencode() {
        String temp = "l";
        
        //temp = this.stream().map((item) -> item.getBencode()).reduce(temp, String::concat);
        for(BencodeDataType item : this) {
            temp += item.getBencode();
        }
     
        temp += "e";
        
        return temp;
    }
    
    @Override
    public byte[] getBencodeBytes() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            out.write("l".getBytes(StandardCharsets.UTF_8));
            for(BencodeDataType item : this) {
                out.write(item.getBencodeBytes());
            }
            out.write("e".getBytes(StandardCharsets.UTF_8));
        } catch (IOException ex) {
            Logger.getLogger(BencodeList.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out.toByteArray();
    }

    @Override
    public String getJSON() {
        String temp = "[";
        //temp = this.stream().map((item) -> item.getJSON() + ',').reduce(temp, String::concat);
        for(BencodeDataType item : this) {
            temp += item.getJSON() + ',';
        }
        temp = temp.substring(0, temp.length() - 1);
        temp += "]";
        return temp;
    }

}
