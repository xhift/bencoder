package com.squgeim.xhiftbencoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author squgeim
 */
public class BencodeString implements BencodeDataType {
    
    private final byte[] value;
    
    public BencodeString(String s) {
        this.value = s.getBytes(StandardCharsets.UTF_8);
    }
    
    public BencodeString(byte[] s) {
        this.value = s;
    }

    @Override
    public String getBencode() {
        return this.value.length + new String(value, StandardCharsets.UTF_8);
    }
    
    @Override
    public byte[] getBencodeBytes() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            out.write((this.value.length + ":").getBytes(StandardCharsets.UTF_8));
            out.write(this.value);
        } catch (IOException ex) {
            Logger.getLogger(BencodeString.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out.toByteArray();
    }
    
    public String getValue() {
        return new String(value, StandardCharsets.UTF_8);
    }
    
    public byte[] getValueBytes() {
        return value;
    }

    @Override
    public String getJSON() {
        return "\"" + new String(value, StandardCharsets.UTF_8) + "\"";
    }
    
}
