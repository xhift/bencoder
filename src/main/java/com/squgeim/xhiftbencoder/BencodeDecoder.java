package com.squgeim.xhiftbencoder;

import com.squgeim.xhiftbencoder.throwables.IsNotBencode;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Stack;

/**
 *
 * @author squgeim
 */
public class BencodeDecoder {

    private byte[] bencodebytes = null;
    private int position = 0;

    public static BencodeDataType decodeBencode(String bencode) throws IOException, IsNotBencode {
        return (new BencodeDecoder(bencode)).getBencodeData();
    }

    public static BencodeDataType decodeBencode(byte[] bencodebytes) throws IOException, IsNotBencode {
        return (new BencodeDecoder(bencodebytes)).getBencodeData();
    }

    public BencodeDecoder(String bencode) {
        this.bencodebytes = bencode.getBytes(StandardCharsets.UTF_8);
    }

    public BencodeDecoder(byte[] bencodebytes) {
        this.bencodebytes = bencodebytes;
    }

    public BencodeDataType getBencodeData() throws IOException, IsNotBencode {
        if (checkSyntax()) {
            return getNextElement();
        } else {
            throw new IsNotBencode();
        }
    }

    private BencodeDataType getNextElement() throws IOException, IsNotBencode {
        if (position > bencodebytes.length) {
            return null;
        }
        switch (bencodebytes[position]) {
            case 'i':
                return getNextInt();
            case 'd':
                return getNextDict();
            case 'l':
                return getNextList();
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                return getNextString();
            case 'e':
                return null;
            default:
                throw new IsNotBencode();
        }
    }

    private BencodeInt getNextInt() throws IsNotBencode {
        byte i = bencodebytes[position++];
        if (i != 'i') {
            throw new IsNotBencode();
        }

        int e;
        for (e = position + 1; e < bencodebytes.length; e++) {
            if (bencodebytes[e] == 'e') {
                break;
            }
            if (e == bencodebytes.length - 1) {
                throw new IsNotBencode();
            }
        }
        String number = new String(Arrays.copyOfRange(bencodebytes, position, e), StandardCharsets.UTF_8);
        position = e + 1;
        System.out.println("Int: " + number);
        return new BencodeInt(new Long(number));
    }

    private BencodeString getNextString() throws IsNotBencode {
        int colon;
        for (colon = position; colon < bencodebytes.length; colon++) {
            if (bencodebytes[colon] == ':') {
                break;
            }
            if (colon == bencodebytes.length - 1) {
                throw new IsNotBencode();
            }
        }
        int length = new Integer(new String(Arrays.copyOfRange(bencodebytes, position, colon), StandardCharsets.UTF_8));

        position = colon + 1;

        byte[] value = Arrays.copyOfRange(bencodebytes, position, position + length);

        position = colon + length + 1;

        return new BencodeString(value);
    }

    private BencodeList getNextList() throws IsNotBencode, IOException {
        BencodeList list = new BencodeList();

        byte l = bencodebytes[position++];
        if (l != 'l') {
            throw new IsNotBencode();
        }

        for (;;) {
            BencodeDataType temp = getNextElement();
            if (temp == null) {
                break;
            }
            list.add(temp);
        }

        byte e = bencodebytes[position++];
        if (e != 'e') {
            throw new IsNotBencode();
        }

        return list;
    }

    private BencodeDictionary getNextDict() throws IsNotBencode, IOException {
        BencodeDictionary dict = new BencodeDictionary();

        byte d = bencodebytes[position++];
        if (d != 'd') {
            throw new IsNotBencode();
        }

        for (;;) {
            BencodeString key = (BencodeString) getNextElement();

            if (key == null) {
                break;
            }

            BencodeDataType value = getNextElement();

            dict.put(key.getValue(), value);
        }

        byte e = bencodebytes[position++];
        if (e != 'e') {
            throw new IsNotBencode();
        }

        return dict;
    }

    public boolean checkSyntax() {
        Stack s = new Stack();
        for (int i = 0; i < bencodebytes.length; i++) {
            byte ch = bencodebytes[i];
            if (ch >= 0x30 && ch <= 0x39) { // is digit
                if (s.empty() ? true : (byte) s.peek() != 'i') {
                    int colon;
                    for (colon = i; colon < bencodebytes.length; colon++) {
                        if (bencodebytes[colon] == ':') {
                            break;
                        }
                    }
                    if (colon == bencodebytes.length) {
                        return false;
                    }
                    String number = new String(Arrays.copyOfRange(bencodebytes, i, colon), StandardCharsets.UTF_8);
                    int length = new Integer(number);
                    i = colon + length;
                    if (i > bencodebytes.length) {
                        return false;
                    }
                    continue;
                }
            }

            if (ch == 'i' || ch == 'd' || ch == 'l') {
                s.push(ch);
            }
            if (ch == 'e' && !s.empty()) {
                s.pop();
            }
        }
        return s.empty();
    }

}
