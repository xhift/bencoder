package com.squgeim.xhiftbencoder;

/**
 *
 * @author squgeim
 */
public interface BencodeDataType {
    
    public String getBencode();
    public String getJSON();
    public byte[] getBencodeBytes();

}
