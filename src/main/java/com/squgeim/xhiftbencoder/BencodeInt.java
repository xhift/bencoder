package com.squgeim.xhiftbencoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author squgeim
 */
public class BencodeInt implements BencodeDataType {
    
    private final long value;
    
    public BencodeInt(int x) {
        this.value = x;
    }
    
    public BencodeInt(long x) {
        this.value = x;
    }

    @Override
    public String getBencode() {
        return "i" + value + "e";
    }
    
    @Override
    public byte[] getBencodeBytes() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            out.write("i".getBytes(StandardCharsets.UTF_8));
            out.write((value+"").getBytes(StandardCharsets.UTF_8));
            out.write("e".getBytes(StandardCharsets.UTF_8));
        } catch (IOException ex) {
            Logger.getLogger(BencodeInt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out.toByteArray();
    }
    
    public long getValue() {
        return value;
    }

    @Override
    public String getJSON() {
        return Long.toString(value);
    }
    
}
