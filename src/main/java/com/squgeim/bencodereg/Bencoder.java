package com.squgeim.bencodereg;

import com.squgeim.xhiftbencoder.BencodeDecoder;
import com.squgeim.xhiftbencoder.throwables.IsNotBencode;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author squgeim
 */
public class Bencoder {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner s = new Scanner( System.in );
//        
//        System.out.println("Enter name: ");
//        String name1 = s.next();
//        System.out.println("Enter age: ");
//        int age1 = s.nextInt();
//        System.out.println("Enter gender: ");
//        String sex1 = s.next();
//        
//        System.out.println("For 2nd person: ");
//        
//        System.out.println("Enter name: ");
//        String name2 = s.next();
//        System.out.println("Enter age: ");
//        int age2 = s.nextInt();
//        System.out.println("Enter gender: ");
//        String sex2 = s.next();
//        
//        BencodeDictionary dict1 = new BencodeDictionary();
//        
//        dict1.put("name", name1);
//        dict1.put("age", age1);
//        dict1.put("sex", sex1);
//
//        BencodeDictionary dict2 = new BencodeDictionary();
//        
//        dict2.put("name", name2);
//        dict2.put("age", age2);
//        dict2.put("sex", sex2);
//        
//        BencodeList l = new BencodeList();
//        l.add(dict1);
//        l.add(dict2);
//        
//        System.out.println("The final bencoded string is: ");
//        System.out.println(l.getBencode());
        
//        BencodeDictionary d = new BencodeDictionary();
//        d.put("month", 4);
//        d.put("name", "april");
//        
//        System.out.println(d.getBencode());

          System.out.println("Enter the bencode: ");
          String bencode = s.next();
          
          System.out.println("Checking for: ");
          System.out.println(bencode);
          
        try {
            System.out.println("Value: " + (BencodeDecoder.decodeBencode(bencode)).getJSON());
        } catch (IOException | IsNotBencode ex) {
            Logger.getLogger(Bencoder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
